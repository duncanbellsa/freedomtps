using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Conditions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Is the target near explosive?")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class IsTargetNearExplosive : Conditional
    {
        [Tooltip("The existing target GameObject")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The distance to check for explosives")]
        [SerializeField] protected SharedFloat m_Radius = 10;
        [Tooltip("Ignore the explosive if it is too close to the agent")]
        [SerializeField] protected SharedFloat m_TooCloseDistance = 5;
        [Tooltip("The layers that the explosive can use")]
        [SerializeField] protected LayerMask m_ExplosiveLayer;
        [Tooltip("The found explosive")]
        [SerializeField] protected SharedGameObject m_Explosive;

        // Internal variables
        private Collider[] m_HitColliders;

        /// <summary>
        /// Initialize the default values.
        /// </summary>
        public override void OnAwake()
        {
#if !(UNITY_5_1 || UNITY_5_2)
            m_HitColliders = new Collider[10];
#endif
        }

        /// <summary>
        /// Returns success if there is an explosive within distance of the agent.
        /// </summary>
        /// <returns>Success if there is an explosive within distance of the agent.</returns>
        public override TaskStatus OnUpdate()
        {
            if (m_Target.Value == null) {
                return TaskStatus.Failure;
            }

            GameObject explosive = null;
            var closestDistance = float.MaxValue;
            var targetTransform = m_Target.Value.transform;
            var hitCount = 0;
#if UNITY_5_1 || UNITY_5_2
            if ((m_HitColliders = Physics.OverlapSphere(m_Target.Value.transform.position, m_Radius.Value, m_ExplosiveLayer)).Length > 0) {
                hitCount = m_HitColliders.Length;
#else
            if ((hitCount = Physics.OverlapSphereNonAlloc(m_Target.Value.transform.position, m_Radius.Value, m_HitColliders, m_ExplosiveLayer)) > 0) {
#endif
                for (int i = 0; i < hitCount; ++i) {
                    // Ignore exposives too close to the current agent.
                    if ((transform.position - m_HitColliders[i].transform.position).magnitude < m_TooCloseDistance.Value) {
                        continue;
                    }

                    // Find the closest explosive to the target.
                    var distance = (targetTransform.position - m_HitColliders[i].transform.position).magnitude;
                    if (distance < closestDistance) {
                        distance = closestDistance;
                        explosive = m_HitColliders[i].gameObject;
                    }
                }
            }

            m_Explosive.Value = explosive;

            return explosive != null ? TaskStatus.Success : TaskStatus.Failure;
        }

        /// <summary>
        /// Reset the Behavior Designer variables.
        /// </summary>
        public override void OnReset()
        {
            m_Target = null;
            m_Radius = 10;
            m_TooCloseDistance = 5;
            m_Explosive = null;
        }
    }
}
