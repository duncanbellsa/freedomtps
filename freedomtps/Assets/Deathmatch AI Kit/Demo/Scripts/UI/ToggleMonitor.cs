﻿using UnityEngine;
using UnityEngine.UI;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Receives the toggle events.
    /// </summary>
    public class ToggleMonitor : MonoBehaviour
    {
        private Toggle m_Toggle;

        /// <summary>
        /// Initialize the component references.
        /// </summary>
        private void Awake()
        {
            m_Toggle = GetComponent<Toggle>();
        }

        /// <summary>
        /// Sets the deathmatch observer mode.
        /// </summary>
        public void SetObserverMode()
        {
            DeathmatchManager.ObserverMode = m_Toggle.isOn;
        }
    }
}