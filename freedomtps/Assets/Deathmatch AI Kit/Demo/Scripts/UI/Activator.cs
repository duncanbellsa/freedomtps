﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Activates the children upon start or pause. 
    /// This is a generic class which is good for activating or deactivating the UI at the start of the game or during a puse.
    /// </summary>
    public class Activator : MonoBehaviour
    {
        [Tooltip("Should the children be activated at game start")]
        [SerializeField] protected bool m_ActivateOnStart = true;
        [Tooltip("Should the children be activated or deactivated when the game is paused?")]
        [SerializeField] protected bool m_ActivateOnPause;
        [Tooltip("Should the GameObject be activated while in observer mode?")]
        [SerializeField] protected bool m_ActivateInObserverMode = true;
        [Tooltip("Should the GameObject be activated while in observer mode?")]
        [SerializeField] protected bool m_UIVisibilityActivation;

        /// <summary>
        /// Registers for any interested events and deactivates all of the children.
        /// </summary>
        private void Awake()
        {
            if (m_ActivateOnStart) {
                EventHandler.RegisterEvent("OnStartGame", StartGame);
            }
            if (m_ActivateOnPause) {
                EventHandler.RegisterEvent<bool>("OnPauseGame", PauseGame);
            }
            if (m_UIVisibilityActivation) {
                EventHandler.RegisterEvent<bool>("OnShowUI", ShowUI);
            }
            EventHandler.RegisterEvent("OnEventHandlerClear", EventHandlerClear);
            if (m_ActivateOnStart || m_ActivateOnPause) {
                ActivateChildren(false);
            }
        }

        /// <summary>
        /// Deactivates the children if requested and in observer mode.
        /// </summary>
        private void Start()
        {
            if (DeathmatchManager.ObserverMode && !m_ActivateInObserverMode) {
                if (m_ActivateOnStart) {
                    EventHandler.UnregisterEvent("OnStartGame", StartGame);
                }
                if (m_ActivateOnPause) {
                    EventHandler.UnregisterEvent<bool>("OnPauseGame", PauseGame);
                }
                // Destroy the object to prevent it from responding to OnShowUI events.
                DestroyImmediate(gameObject);
            }
        }

        /// <summary>
        /// The game has started. Activate the children.
        /// </summary>
        private void StartGame()
        {
            EventHandler.UnregisterEvent("OnStartGame", StartGame);

            ActivateChildren(true);
        }

        /// <summary>
        /// The game has been paused. Activator or deactivate the children.
        /// </summary>
        /// <param name="pause">Was the game paused?</param>
        private void PauseGame(bool pause)
        {
            ActivateChildren(pause);
        }

        /// <summary>
        /// Shows or hides the UI.
        /// </summary>
        /// <param name="show">Should the UI be shown?</param>
        private void ShowUI(bool show)
        {
            ActivateChildren(show);
        }

        /// <summary>
        /// Loops through the children and sets the active state.
        /// </summary>
        /// <param name="activate">Should the child GameObject be activated?</param>
        private void ActivateChildren(bool activate)
        {
            for (int i = 0; i < transform.childCount; ++i) {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(activate);
            }
        }

        /// <summary>
        /// The EventHandler was cleared. This will happen when a new scene is loaded. Unregister the registered events to prevent old events from being fired.
        /// </summary>
        private void EventHandlerClear()
        {
            if (m_ActivateOnPause) {
                EventHandler.UnregisterEvent<bool>("OnPauseGame", PauseGame);
            }
            if (m_ActivateOnStart) {
                EventHandler.UnregisterEvent("OnStartGame", StartGame);
            }
            if (m_UIVisibilityActivation) {
                EventHandler.UnregisterEvent<bool>("OnShowUI", ShowUI);
            }
            EventHandler.UnregisterEvent("OnEventHandlerClear", EventHandlerClear);
        }

        /// <summary>
        /// The object has been destroyed - unregister for all events.
        /// </summary>
        private void OnDestroy()
        {
            EventHandlerClear();
        }
    }
}