﻿using UnityEngine;
using Opsive.ThirdPersonController;
using Opsive.DeathmatchAIKit.Abilities;
using System.Collections.Generic;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Applies a vertical force to the Rigidbody similar to the object being on an air lift.
    /// </summary>
    public class AirLift : MonoBehaviour
    {
        [Tooltip("The force to apply to the Rigidbody")]
        [SerializeField] protected float m_Force = 20;

        // Internal variables
        private float m_CapsuleColliderTop;

        // Component references
        private AudioSource m_AudioSource;
        private List<Rigidbody> m_ActiveRigidbodies = new List<Rigidbody>();

        /// <summary>
        /// Cache the component references and initialize the default values.
        /// </summary>
        private void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
            var capsuleCollider = GetComponent<CapsuleCollider>();
            m_CapsuleColliderTop = transform.position.y + capsuleCollider.center.y + capsuleCollider.height / 2;

            // The component does not need to be active until there is an object within the trigger.
            enabled = false;
        }

        /// <summary>
        /// Apply a force to all of the Rigidbodies.
        /// </summary>
        private void FixedUpdate()
        {
            // There may be multiple objects on the lift in which case the force should apply to all of them.
            if (m_ActiveRigidbodies.Count > 0) {
                for (int i = m_ActiveRigidbodies.Count - 1; i > -1; --i) {
                    // Remove the Rigidbody if it is above the top of the capsule collider. OnTriggerExit isn't always reliable.
                    if (m_ActiveRigidbodies[i].position.y > m_CapsuleColliderTop ) {
                        m_ActiveRigidbodies.RemoveAt(i);
                    } else {
                        // Apply an upwards force to any Rigidbody within the trigger.
                        m_ActiveRigidbodies[i].AddForce(Vector3.up * m_Force);
                    }
                }

                // The component no longer needs to update if there are no more Rigidbodies to apply a force to.
                if (m_ActiveRigidbodies.Count == 0) {
                    enabled = false;
                }
            }
        }

        /// <summary>
        /// An object has entered the trigger.
        /// </summary>
        /// <param name="other">The object that entered the trigger.</param>
        private void OnTriggerEnter(Collider other)
        {
            // The lift can't do anything if there is no Rigidbody attached to the object.
            if (other.attachedRigidbody == null) {
                return;
            }

            // Do not add the Rigidbody to the list twice.
            var controller = Utility.GetComponentForType<RigidbodyCharacterController>(other.gameObject, true);
            var activeRigidbody = (controller == null ? other.attachedRigidbody : Utility.GetComponentForType<Rigidbody>(controller.gameObject));
            if (m_ActiveRigidbodies.Contains(activeRigidbody)) {
                return;
            }

            var health = Utility.GetComponentForType<Health>(other.gameObject, true);
            // Do not add the character to the lift if they are dead.
            if (health != null && health.CurrentHealth == 0) {
                return;
            }

            enabled = true;
            m_ActiveRigidbodies.Add(activeRigidbody);
            m_AudioSource.Play();

            // If the object that entered the trigger is a Third Person Controller character then start the air lift ability.
            UseAirLift liftAbility;
            if (controller != null && (liftAbility = Utility.GetComponentForType<UseAirLift>(activeRigidbody.gameObject)) != null) {
                controller.TryStartAbility(liftAbility, true);
                liftAbility.AirLift = this;
            }
        }

        /// <summary>
        /// The character's Air Lift ability has ended. Remove the character from the lift.
        /// </summary>
        /// <param name="character">The character that should be removed from the air lift.</param>
        public void RemoveCharacter(GameObject character)
        {
            var characterRigidbody = Utility.GetComponentForType<Rigidbody>(character);
            m_ActiveRigidbodies.Remove(characterRigidbody);
            // The component no longer needs to update if there are no more Rigidbodies to apply a force to.
            if (m_ActiveRigidbodies.Count == 0) {
                enabled = false;
            }
        }
    }
}