﻿using UnityEngine;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Input;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Handles input for the Deathmatch AI Kit observer camera.
    /// </summary>
    public class ObserverCameraHandler : MonoBehaviour
    {
        // Component references
        private ObserverCamera m_ObserverCamera;
        private PlayerInput m_PlayerInput;

        /// <summary>
        /// Cache the component references and register for an interested events.
        /// </summary>
        private void Awake()
        {
            m_ObserverCamera = GetComponent<ObserverCamera>();
            m_PlayerInput = GetComponent<PlayerInput>();

            EventHandler.RegisterEvent<bool>("OnPauseGame", PauseGame);
        }

        /// <summary>
        /// Update the input and move the camera.
        /// </summary>
        private void Update()
        {
            m_ObserverCamera.Move(m_PlayerInput.GetAxis(Constants.HorizontalInputName), m_PlayerInput.GetAxis(Constants.ForwardInputName), 
                                    m_PlayerInput.GetAxis(Constants.PitchInputName), m_PlayerInput.GetAxis(Constants.YawInputName));
        }

        /// <summary>
        /// The game has been paused. Activator or deactivate the children.
        /// </summary>
        /// <param name="pause">Was the game paused?</param>
        private void PauseGame(bool pause)
        {
            enabled = !pause;
        }

        /// <summary>
        /// Unregister for any events that the camera was registered for.
        /// </summary>
        private void OnDestroy()
        {
            EventHandler.UnregisterEvent<bool>("OnPauseGame", PauseGame);
        }
    }
}