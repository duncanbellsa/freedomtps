﻿using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Abilities;

namespace Opsive.DeathmatchAIKit.Abilities
{
    /// <summary>
    /// The Use Air Lift ability will allow the character to use a Deathmatch Kit air lift.
    /// </summary>
    public class UseAirLift : Ability
    {
        // Internal variables
        private AirLift m_AirLift;

        public AirLift AirLift { set { m_AirLift = value; } }

        /// <summary>
        /// Starts executing the ability.
        /// </summary>
        protected override void AbilityStarted()
        {
            m_Controller.StopMovement();

            base.AbilityStarted();
            EventHandler.RegisterEvent(m_GameObject, "OnDeath", OnDeath);
        }

        /// <summary>
        /// Unregister for any events that the ability was registered for.
        /// </summary>
        protected override void AbilityStopped()
        {
            m_AirLift.RemoveCharacter(m_GameObject);
            m_AirLift = null;
            base.AbilityStopped();
            EventHandler.UnregisterEvent(m_GameObject, "OnDeath", OnDeath);
        }

        /// <summary>
        /// Returns the destination state for the given layer.
        /// </summary>
        /// <param name="layer">The Animator layer index.</param>
        /// <returns>The state that the Animator should be in for the given layer. An empty string indicates no change.</returns>
        public override string GetDestinationState(int layer)
        {
            // The ability only affects the base and upper layers.
            if (m_Controller.Grounded || (layer != m_AnimatorMonitor.BaseLayerIndex && layer != m_AnimatorMonitor.UpperLayerIndex && !m_AnimatorMonitor.ItemUsesAbilityLayer(this, layer))) {
                return string.Empty;
            }

            return "Air Lift.Use";
        }

        /// <summary>
        /// Can the ability be stopped?
        /// </summary>
        /// <returns>True if the ability can be stopped.</returns>
        public override bool CanStopAbility()
        {
            return m_Controller.Grounded && m_Controller.Velocity.y < -0.5f;
        }

        /// <summary>
        /// The character has died.
        /// </summary>
        private void OnDeath()
        {
            StopAbility(true);
        }
    }
}