﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_play_sound_from_list : MonoBehaviour {

	public AudioClip[] DieSounds;
	private AudioSource source;


	// Use this for initialization

	void OnEnable (){




	}

	void Start () {
		AudioSource source = GetComponent<AudioSource>();


		source.pitch = (Random.Range(0.90f,1.10f));
		source.PlayOneShot(DieSounds[Random.Range(0,DieSounds.Length)]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
