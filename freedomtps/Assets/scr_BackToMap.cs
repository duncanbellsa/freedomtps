﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_BackToMap : MonoBehaviour {

	public GameObject GameManager;
    public GameObject PlayerObject;
    public int MinDistance;
    private bool ShowContext = false;
    private bool Canexit = false;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        

        if ((Vector3.Distance(PlayerObject.transform.position, transform.position) > MinDistance) && (Vector3.Distance(PlayerObject.transform.position, transform.position) < (MinDistance + 2)))
        {
            Canexit = false;
            scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
            sn.HideContextText();
            
        }
        else if (Vector3.Distance(PlayerObject.transform.position, transform.position) < MinDistance )
        {
            Canexit = true;
            scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
            sn.ShowContextTextBuggy();
            
        }
        




        if (Input.GetButtonUp("Action") && Canexit == true){
            scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
			sn.HideContextText();
            sn.ChangeToMapFromCamp();
		}
	}

	 
	/*
    void OnTriggerEnter(Collider other)
	{
		
		if (other.gameObject.tag == "Player")
		{
			Canexit = true;

			
		}

	}

	void OnTriggerExit(Collider other)
	{

		if (other.gameObject.tag == "Player")
		{
			Canexit = false;


		}

	}
    */


}
