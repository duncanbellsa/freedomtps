﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class RandomTerrainGenerator_Object : MonoBehaviour {

	//The higher the numbers, the more hills/mountains there are
	private float HM;

	//The lower the numbers in the number range, the higher the hills/mountains will be...
	private float divRange;

	//[MenuItem("Terrain/Generate Random Terrain")]
	//public static void CreateWizard(MenuCommand command)
	//{
		//ScriptableWizard.DisplayWizard("Generate Random Terrain", typeof(RandomTerrainGenerator));
	//}


	public GameObject Player;

	public int numberOfCamps;
	public GameObject[] typeOfCamps;
	public GameObject EnemyCampEscapedFrom;
    //public GameObject HomeBase;


	public GameObject[] Rocks;
	public int NumberOfRocks;


    void Awake()
	{
        if (scr_ResourceManagement.StartNewGame == true)
        {
            HM = Random.Range(12, 20);
            divRange = Random.Range(12, 25);

            //Generate Terrain
            GameObject G = this.gameObject;
            if (G.GetComponent<Terrain>())
            {
                GenerateTerrain(G.GetComponent<Terrain>(), HM);
            }
            
        }
       

	}

    public void Start()
    {
        if (scr_ResourceManagement.StartNewGame == true)
        {
            // Align vehicle to terrain
            Ray ray = new Ray(Player.transform.position, Vector3.down);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);

            }

            Player.transform.position = hit.point;

            GenerateRocks();

            // Generate Camps
            GenerateCamps();

			


            //Turn off random generation until user goes back to main menu
            
        }
        scr_ResourceManagement.StartNewGame = false;
    }



	private int RandomX; 
	private int RandomZ;
	private int RandomCamp;
    private int RandomCampToSpawn;


	public GameObject CampGroupObject;
	public GameObject RockGroupObject;
    public GameObject RockLocation;

    public void GenerateCamps(){



		for (int i = 0; i < numberOfCamps; i++) {

            //Generate a random location
            RandomX = Random.Range(300,1748);
			RandomZ = Random.Range(300,1748);

            // Select a random camp
            RandomCamp = Random.Range (0, 100);
            if( RandomCamp < 20) { 
                RandomCampToSpawn = 0;
            }
            if (RandomCamp >= 20 && RandomCamp < 40) { 
                 RandomCampToSpawn = 1;
            }
            if (RandomCamp >= 40)
            {
                RandomCampToSpawn = 2;
            }

            //Cast ray down and ensure camp is placed on terrain
            RockLocation.transform.position = new Vector3(RandomX, 50, RandomZ);

            Ray ray = new Ray(RockLocation.transform.position, Vector3.down);
            RaycastHit hit;


            if (Physics.Raycast(ray, out hit, 1000) && hit.transform.tag == "Terrain")
            {

                Debug.DrawLine(ray.origin, hit.point);
                RockLocation.transform.position = hit.point;
                RockLocation.transform.up = hit.normal;
                Debug.Log(RandomX.ToString() + RandomZ.ToString());
                //(Instantiate(typeOfCamps[RandomCampToSpawn], new Vector3(RandomX, 100, RandomZ), transform.rotation) as GameObject).transform.parent = CampGroupObject.transform;
                Instantiate(typeOfCamps[RandomCampToSpawn], new Vector3(hit.point.x, hit.point.y + 1.25f, hit.point.z), RockLocation.transform.rotation).transform.parent = CampGroupObject.transform;
                
            }
            else
            {
                
                i -= 1;
            }

            //Instantiate(typeOfCamps[RandomCampToSpawn],new Vector3(RandomX,100,RandomZ), transform.rotation);
        }

        


        


        Instantiate(EnemyCampEscapedFrom,new Vector3(256,100,256), transform.rotation);
        //Instantiate(HomeBase, new Vector3(1750, 100, 1750), transform.rotation);
    }


	

	public void GenerateRocks(){


		for (int i = 0; i < NumberOfRocks; i++) {
			RandomX = Random.Range(300,1748);
			RandomZ = Random.Range(300,1748);



			RandomCamp = Random.Range (0, Rocks.Length);

			RockLocation.transform.position = new Vector3 (RandomX, 50, RandomZ);

            Ray ray = new Ray(RockLocation.transform.position, Vector3.down);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000) && hit.transform.tag == "Terrain")
            {
                
                Debug.DrawLine(ray.origin, hit.point);
                RockLocation.transform.position = hit.point;
                RockLocation.transform.up = hit.normal;
                (Instantiate(Rocks[RandomCamp], RockLocation.transform.position, RockLocation.transform.rotation) as GameObject).transform.parent = RockGroupObject.transform;
            }
            else i -= 1;

			


		}

	}



    private float seed;
	//Our Generate Terrain function
	public void GenerateTerrain(Terrain t, float tileSize)
	{

        seed = Random.Range(0, 10000);
        //Mathf.PerlinNoise (seed, seed);

        //Heights For Our Hills/Mountains
        float[,] hts = new float[t.terrainData.heightmapWidth, t.terrainData.heightmapHeight];
		for (int i = 0; i < t.terrainData.heightmapWidth; i++)
		{
			for (int k = 0; k < t.terrainData.heightmapHeight; k++)
			{
				hts[i, k] = Mathf.PerlinNoise(seed + ((float)i / (float)t.terrainData.heightmapWidth) * tileSize, seed + ((float)k / (float)t.terrainData.heightmapHeight) * tileSize)/ divRange;
			}
		}
		Debug.LogWarning("DivRange: " + divRange + " , " + "HTiling: " + HM);
		t.terrainData.SetHeights(0, 0, hts);

       
    }



}
