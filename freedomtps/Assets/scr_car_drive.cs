﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scr_car_drive : MonoBehaviour {

	public float speed = 10.0F;
	public float rotationSpeed = 100.0F;

	public GameObject leftTyre;
	public GameObject rightTyre;
	public GameObject rearLeftTyre;
	public GameObject rearRightTyre;


    //UI Stuffs
    public GameObject PausePlay;
    //public Text ResourceFuelText;


    






    // Use this for initialization
    void Start () {

        


	}
		


	void Update() {
		float translation = Input.GetAxis("Vertical") * speed;
		float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
		translation *= Time.deltaTime;
		rotation *= Time.deltaTime;


		if (scr_ResourceManagement.ResourceFuel > 0){
			transform.Translate(0, 0, translation);
		}
		if (scr_ResourceManagement.ResourceFuel <= 0){
			transform.Translate(0, 0, translation/3);
		}

		//If driving forward
		if (translation > 0) {

            //Update UI
            PausePlay.SetActive(false);

			//Time back to 1
			Time.timeScale = 1f;

			//Rotate wheels
			transform.Rotate (0, rotation, 0);
			rearLeftTyre.transform.localEulerAngles += new Vector3 ((translation * 15), 0, 0);
			rearRightTyre.transform.localEulerAngles += new Vector3 ((translation * 15), 0, 0);


			//Subtract Fuel
			if (scr_ResourceManagement.ResourceFuel > 0) {
				scr_ResourceManagement.ResourceFuel -= 0.025f;
			} else
				scr_ResourceManagement.ResourceFuel = 0;



		} 

 
		//If driving backware
		else if (translation < 0) {

            //Update UI
            PausePlay.SetActive(false);

            //Time back to 1
            Time.timeScale = 1f;

			//Rotate wheels
			transform.Rotate (0, -rotation, 0);
			rearLeftTyre.transform.localEulerAngles += new Vector3 (-(translation * 15), 0, 0);
			rearRightTyre.transform.localEulerAngles += new Vector3 (-(translation * 15), 0, 0);

			//Subtract Fuel
			if (scr_ResourceManagement.ResourceFuel > 0) {
				scr_ResourceManagement.ResourceFuel -= 0.025f;
			} else
				scr_ResourceManagement.ResourceFuel = 0;

		}

		//Stopping
		else {

			//Time back to 0
			transform.Rotate (0, 0, 0);
			Time.timeScale = 0.0001f;

            //Update UI
            PausePlay.SetActive(true);
        }

		leftTyre.transform.localEulerAngles = new Vector3(0,0,(rotation * 15));
		rightTyre.transform.localEulerAngles = new Vector3(0,0,(rotation * 15));
		
	}

    void LateUpdate()
    {
        //Update UI
        

    }
    


}

