﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_marker : MonoBehaviour {

    public int spin;
    public bool DestroyOnceDone;
    public Color fade;
    public float fadespeed;

	// Use this for initialization
	void Start () {
        
        fade.a = 1f;
    }
	
	// Update is called once per frame
	void Update () {

        if (fade.a > 0f)
        {
            fade.a -= fadespeed;
        }
        else if (DestroyOnceDone == true)
        {
            Destroy(gameObject);
        }
        else this.gameObject.active = false;

        this.GetComponent<MeshRenderer>().material.color = new Color(fade.r, fade.g, fade.b, fade.a);


        transform.Rotate(spin * Vector3.up * Time.deltaTime);

        
       
        

    }
}

