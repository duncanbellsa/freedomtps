﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AC.TimeOfDaySystemFree;

public class scr_ResourceManagement : MonoBehaviour {

	public static float ResourceFuel = 100f;
	public static float ResourceMoney = 100f;
    public static bool StartNewGame = true;

    public static float CampFuel = 0f;

    public Text ResourceFuelLabel;
	public Text ResourceMoneyLabel;

    public Text CampFuelLabel;

	public TimeOfDayManager TOD_Manager;

    public GameObject ContextTextObj;
    public Text ContextTextText;
   

    // Use this for initialization
    void Start () {
		
		DontDestroyOnLoad(this);
        


    }
	
	// Update is called once per frame
	void Update () {



		ResourceFuelLabel.text = (ResourceFuel.ToString("F1")) ;
        CampFuelLabel.text = (CampFuel.ToString("F1"));
        ResourceMoneyLabel.text = (ResourceMoney.ToString("F1"));

        // CHEATS
        if (Input.GetKeyUp(KeyCode.F11))
            ResourceFuel += 100f;

        if (Input.GetKeyUp(KeyCode.F12))
            ResourceMoney += 100f;

        
        if (Input.GetMouseButtonDown(2))
        {
            Time.timeScale = 0.5f;
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
        }

        if (Input.GetMouseButtonUp(2))
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02F;
        }
        

        }



    // Different scene objects
    public GameObject MapObject;
    public GameObject CampObject;
	public GameObject MarketObject;
    public GameObject LoadingScreen;

	private float DayInSecondsTime;

	public void ChangeToCamp()
	{
		//AC.TimeOfDaySystemFree.TimeOfDay.dayInSeconds = 10f;

		TOD_Manager.playTime = false;
		LoadingScreen.SetActive (true);
		MapObject.SetActive (false);
		CampObject.SetActive (true);
		LoadingScreen.SetActive (false);
	}

    public void ChangeToMarket()
    {

		TOD_Manager.playTime = false;


		LoadingScreen.SetActive (true);
        MapObject.SetActive(false);
        MarketObject.SetActive(true);
		LoadingScreen.SetActive (false);
    }


    private GameObject CamptoDelete;

	public void ChangeToMapFromCamp()
	{
		TOD_Manager.playTime = true;

		LoadingScreen.SetActive (true);
		CampObject.SetActive (false);

		//CamptoDelete = GameObject.Find("CampHolder");
		GameObject.Destroy(GameObject.Find("CampHolder"));

		GameObject[] EnemiesToDelete = GameObject.FindGameObjectsWithTag("Enemy");

		for (int i=0; i<EnemiesToDelete.Length; i++){
			Destroy(EnemiesToDelete[i]);
		}

		MapObject.SetActive (true);
		LoadingScreen.SetActive (false);
	}

    public void ChangeToMapFromMarket()
    {
		TOD_Manager.playTime = true;

		LoadingScreen.SetActive (true);;
        MarketObject.SetActive(false);
        MapObject.SetActive(true);
		LoadingScreen.SetActive (false);
    }

    public void ShowContextTextBuggy()
    {
        ContextTextText.text = "Press F to get back in buggy";
        ContextTextObj.SetActive(true);
    }

    public void ShowContextTextContainer()
    {
        ContextTextText.text = "Press F to drop or collect fuel can";
        ContextTextObj.SetActive(true);
    }

    public void HideContextText()
    {
        ContextTextText.text = "";
        ContextTextObj.SetActive(false);
    }





}
