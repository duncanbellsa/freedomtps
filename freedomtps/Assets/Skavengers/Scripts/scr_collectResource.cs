﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_collectResource : MonoBehaviour {

    public float CollectSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (scr_ResourceManagement.CampFuel > CollectSpeed)
        {
            scr_ResourceManagement.ResourceFuel += CollectSpeed;
            scr_ResourceManagement.CampFuel -= CollectSpeed;
        }

		
	}
}
