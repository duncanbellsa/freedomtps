﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PlayerCollection : MonoBehaviour {

    public bool HasCollectionDevice = true;
    public GameObject CollectionDevicePrefab;
    public GameObject PlayerCollectionCastOrigin;
    //public Collider CollectionPoint;
    private bool insideCollectionArea = false;

    public GameObject GameManager;





    // Use this for initialization
    void Start () {
        
	}

    // Update is called once per frame
    void Update() {

        
          
		// Check if inside collection area and if button is pressed

        if ((Input.GetButtonUp("Action")) & (insideCollectionArea == true))
        {
            
            // Check if the player has the collection device on them
            if ((HasCollectionDevice == true)) //& (counter < 1))
            { 

                Ray raydown = new Ray(PlayerCollectionCastOrigin.transform.position, Vector3.down);
            
            RaycastHit hit;


            if (Physics.Raycast(raydown, out hit, 10))
            {
                Debug.DrawRay(raydown.origin, hit.point);
                

            }

            // Place device on floor
            
            Instantiate(CollectionDevicePrefab, hit.point, transform.rotation);
            HasCollectionDevice = false;
            Debug.Log("Planted");
            
            }


			// else player needs to pick up the device
            else
				
            {
                
                GameObject.Destroy(GameObject.FindGameObjectWithTag("Collection_Object"));


                HasCollectionDevice = true;
                Debug.Log("Picked Up");
                

            }
        }

        
    }




    void OnTriggerEnter(Collider other)
    {
		if (other.GetComponent<Collider>().gameObject.name == "Resource_Trigger") {
			insideCollectionArea = true;
			Debug.Log ("Player is inside Area");


            scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
            sn.ShowContextTextContainer();
        }
        
    }

    void OnTriggerExit(Collider other)
    {
		if (other.GetComponent<Collider>().gameObject.name == "Resource_Trigger") {
			insideCollectionArea = false;
			Debug.Log ("Player has exited Area");

            scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
            sn.HideContextText();
        }

    }

}

