﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class scr_RandomLayout : MonoBehaviour {

	//Initalization variables to setup room

	public int gridsize;    //gridsize is how wide the tiles are

    [Header("CampFuel Label")]
    //Arrays of GameObjects that can spawn in areas

    public GameObject CampFuelLabelObj;


    [Header("Randomzie size of level?")]
	public bool RandomSize;

	[Header("Debug Tools")]
	public bool GenerateWalls;
	public bool GenerateBigCover;
	public bool GenerateSmallCover;

	[Header("Minimum and maximum number of tiles")]
	public int RandomMin;
	public int RandomMax;

	[Header("If random size isn't true, how big should the room be?")]
	public int RoomWidth;
	public int RoomHeight;


	[Header("Level Elements")]
	//Arrays of GameObjects that can spawn in areas

	public GameObject Fence;

	public GameObject[] Walls;
	public GameObject[] SmallCover;
	public GameObject[] BigCover;
	public GameObject RefuelStation;


	//Used in While loops to get coordinates
	private int w = 0;
	private int h = 0;
	private int y = 100;
	private int whilew = 0;
	private int whileh = 0;
	private int RandomNum = 0;
	private int DoSpawnWall = 0;

	//Used to find if the refuel station has been created yet
	private bool RefuelSpawned = false;



	// Use this for initialization
	void Start () {
		/*
          
         if (RandomSize == true)
            
            {
            RoomHeight = Random.Range(RandomMin, RandomMax);
            RoomWidth = Random.Range(RandomMin, RandomMax);
             }
        RandomizeLayout();

		*/






	}

	// Update is called once per frame
	void Update () {


	}

	void OnEnable(){

        //Randomly Generate Fuel Amount

        RefuelSpawned = false;

        scr_ResourceManagement.CampFuel = Random.Range(20.00f, 150.00f);

        CampFuelLabelObj.active = true;

        if (RandomSize == true)

		{
			RoomHeight = Random.Range(RandomMin, RandomMax);
			RoomWidth = Random.Range(RandomMin, RandomMax);
		}
		RandomizeLayout();

	}

    void OnDisable()
    {
		CampFuelLabelObj.active = false;

    }



	void RandomizeLayout()
	{

		w = 0;
		h = 0;
		y = 100;

		//Reset campholder object to 0,0,0

		//CampHolder.name = "CampHolder";
		GameObject CampHolder = new GameObject();
		CampHolder.name = "CampHolder";

		CampHolder.transform.position = new Vector3(0,0,0);





		//Generate wall bounds, using max coordinates

		if (GenerateWalls == true) {
			while (w < (RoomWidth + 1))
			{
				while (h < (RoomHeight + 1)) {
					//Align coordinates to gridsize of blocks
					whilew = w * gridsize;
					whileh = h * gridsize;

					//Create wall prefab at the edges of the room
					if (w == 0 || h == 0 || h == RoomHeight || w == RoomWidth) {

						//Generate random number to generate a wall object or not
						DoSpawnWall = Random.Range (0, 10);
						if (DoSpawnWall < 5) {
							(Instantiate (Walls [Random.Range (0, Walls.Length)], new Vector3 (whilew, y, whileh), Quaternion.identity) as GameObject).transform.parent = CampHolder.transform;
						}
					}
					h += 1;
				}

				w += 1;
				h = 0;

			}
		}



		w = 0;
		h = 0;



		//Generate small cover
		if (GenerateBigCover == true) {
			while ((w < (RoomWidth + 1))) {
				while (h < (RoomHeight + 1)) {
					//Align coordinates to gridsize of blocks
					whilew = w * gridsize;
					whileh = h * gridsize;



					//Create small cover prefabs towards the outskirts
					if (w == 1 || h == 1 || h == (RoomHeight - 1) || w == (RoomWidth - 1))  {

						if ((w != 0) && (h != 0) && (w != RoomWidth) && (h != RoomHeight))
						{
						(Instantiate (SmallCover [Random.Range (0, SmallCover.Length)], new Vector3 (whilew, y, whileh), Quaternion.identity)  as GameObject).transform.parent = CampHolder.transform;
						}
					}
					h += 1;
				}

				w += 1;
				h = 0;

			}


		}
		w = 0;
		h = 0;



		//Generate cover in centre of map

		if (GenerateSmallCover == true) {
			while ((w < (RoomWidth + 1))) {
				while (h < (RoomHeight + 1)) {
					//Align coordinates to gridsize of blocks
					whilew = w * gridsize;
					whileh = h * gridsize;
					RandomNum = Random.Range (0, 11);

					//Create big cover prefab at the centre of the room
					if (w > 1 && w < (RoomWidth - 1) && h > 1 && h < (RoomHeight - 1)) {

						//Spawn refuel station
						if (RefuelSpawned == false) {
							(Instantiate (RefuelStation, new Vector3 (whilew, y, whileh), Quaternion.identity) as GameObject).transform.parent = CampHolder.transform;
							RefuelSpawned = true;
						}

					//Spawn the cover as usual
					else if (RandomNum >= 5) {
							(Instantiate (BigCover [Random.Range (0, BigCover.Length)], new Vector3 (whilew, y, whileh), Quaternion.identity) as GameObject).transform.parent = CampHolder.transform;
						} else
							(Instantiate (SmallCover [Random.Range (0, SmallCover.Length)], new Vector3 (whilew, y, whileh), Quaternion.identity) as GameObject).transform.parent = CampHolder.transform;
					}


					h += 1;
				}

				w += 1;
				h = 0;

			}

		}
		w = 0;
		h = 0;





		//Rotate the holding object to be random
		//CampHolder.transform.Rotate(0, Random.Range(0,360), 0);

		Debug.Log ("Height Before: " + RoomHeight); 
		Debug.Log ("Height Before: " + RoomWidth); 
		//Move the holding camp object to the middle of the map
		RoomHeight *=5;
		RoomWidth *= 5;
		Debug.Log ("Height After: " + RoomHeight); 
		Debug.Log ("Height After: " + RoomWidth); 
		CampHolder.transform.position = new Vector3 (-RoomHeight, 25, -RoomWidth);
		//CampHolder.transform.RotateAround(Vector3.zero, Vector3.up, Random.Range(0,360));
	}
}
