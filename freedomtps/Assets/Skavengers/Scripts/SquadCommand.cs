﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SquadCommand : MonoBehaviour
{

    public AudioClip FollowMe;
    public AudioClip Go;
    AudioSource audio;

	public GameObject AllyGameObject;
    public int NumberOfAllys;

    public GameObject marker;
    public GameObject FollowArrow;
    public bool SquadFollow;
	public bool SquadFollowCommand;
    private float distance;
    private float new_distance;
    private Vector3 point;
	public GameObject[] AgentGameObjects;
    public UnityEngine.AI.NavMeshAgent[] agent;

    private int activeAgent;
    public Transform raycast_position;
	private int i = 0;
	private int timerwait = 0;


	public GameObject[] SpawnPoints;
	public GameObject BuggyObject;
	private int SelectedSpawnPoint;

	void OnEnable()
	{
		FollowArrow.active = false;
		SquadFollow = false;
		SquadFollowCommand = false;
		activeAgent = 0;
		audio = GetComponent<AudioSource>();

		SelectedSpawnPoint = Random.Range (0, 3);

		transform.position = SpawnPoints [SelectedSpawnPoint].transform.position;
		BuggyObject.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z + 15);


		//move allys if they exist

		i = 0;

		while ( i < AgentGameObjects.Length)
		{

			AgentGameObjects[i].transform.position = new Vector3 (transform.position.x+(i*3), 50, transform.position.z);
			i++;
		}

	}

	void OnDisable()
	{
		
	}



	void Start()
    {
		//SPAWN Ally GameObjects


		i = 0;
		//AgentGameObjects.Length = NumberOfAllys;
		//agent.Length = NumberOfAllys;

		while ( i < AgentGameObjects.Length)
		{

			AgentGameObjects[i] = Instantiate (AllyGameObject, new Vector3(transform.position.x, transform.position.y, transform.position.z + ((i+2)*2)), this.transform.rotation) as GameObject;
			i++;
		}





		//Put the allys into an array

		i = 0;
		while( i < AgentGameObjects.Length)
		{
			agent[i] = AgentGameObjects[i].GetComponent<UnityEngine.AI.NavMeshAgent>();
			//AgentGameObjects[i] = Instantiate (AllyGameObject, this.transform.position, this.transform.rotation) as GameObject;
			i++;
		}

    }

    void Update()
    {
        

        if (Input.GetButtonUp("CommandFollow"))
        {
            SquadFollow = true;
			SquadFollowCommand = true;
            FollowArrow.active = true;
            audio.PlayOneShot(FollowMe, 0.7F);
            StartCoroutine (wait());

            
        }


        
        if (SquadFollow == true)
        {
			
			// Seats a break so the follow command isn't called every frame.
			timerwait += 1;
			if (timerwait == 15)
			{
				// Once timer is hit, then it calls all solders to regroup
				while (i < (agent.Length)) 
				{
					
					agent[i].stoppingDistance = 2;
					agent[i].SetDestination (transform.position);
					Debug.Log ("Agent" + i + "told to regroup");
					i += 1;

				}
				//Reset while loop for next time
				i = 0;
				//Resets the timer to start again
				timerwait = 0;
			}
        }
        


        if (Input.GetButtonUp("CommandDefend"))
        {
            SquadFollow = false;
			SquadFollowCommand = false;
			i = 0;
            audio.PlayOneShot(Go, 0.7F);
            

			//CAST RAY TO GET POSITION

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//Ray ray = new Ray(Camera.main.transform.position,Camera.main.transform.forward);  - This is interfereing with the player
            RaycastHit hit;
            

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
            }

            
            //CAST RAY, SUBTRACTING DISTANCE TO GET BUFFER
            distance = Vector3.Distance(Camera.main.transform.position, hit.point);

            new_distance = distance - 1;
            if (new_distance < 0) {
                new_distance = 0;

            }

            Ray raydistance = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(raydistance, out hit, new_distance))
            {
                
                Debug.DrawLine(ray.origin, hit.point);
            }
            point = raydistance.origin + (raydistance.direction * new_distance);
            

            //CAST RAY DOWN TO GET VERTICAL POSITION

            Ray raydown = new Ray(point, Vector3.down);
            
            if (Physics.Raycast(raydown, out hit, 100))
            {
                Debug.DrawLine(raydown.origin, hit.point);

            }

    

            


            ///Create marker
            Instantiate(marker, hit.point, Quaternion.identity);

            //Tell guy to go there

            agent[activeAgent].SetDestination(hit.point);
			agent[activeAgent].stoppingDistance = 0.5f;
            if (activeAgent < (agent.Length-1))
            {
                activeAgent += 1;
                
            } else
                activeAgent = 0;
            print(activeAgent);

            raycast_position.position = hit.point;

        }
    }



IEnumerator wait()
    {
        Debug.Log("Wait starts");
        yield return new WaitForSeconds(1f);
        FollowArrow.active = false;
        Debug.Log("Wait ends");
    }
}

