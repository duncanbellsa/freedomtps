﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_random_rotation : MonoBehaviour {

    public bool Rotation360;
	[SerializeField] private int Random_Rotation;
	[SerializeField] private int Random_Rotation360;
	[SerializeField] private int Random_RotationMultiplier;

	private bool HasRotationBeenSet;

	// Use this for initialization
	void OnEnable () {
		

		Random_RotationMultiplier = Random.Range(0, 4);
		Random_Rotation = Random_RotationMultiplier * 90;
		Random_Rotation360 = Random.Range (0, 360);
		HasRotationBeenSet = false;



        
    }

	void Update ()
	{
		if (HasRotationBeenSet == false) {
		
			SetRotation ();
			HasRotationBeenSet = true;
		}
	}



    // Update is called once per frame
    void SetRotation () {
		
		if (Rotation360 == true)  
		{

			//transform.Rotate(0, Random_Rotation360, 0);
			this.transform.localEulerAngles = new Vector3(transform.rotation.x,Random_Rotation360,transform.rotation.z);
		}
		else 
		{
			//Random_RotationMultiplier = Random.Range(0, 4);
			//Random_Rotation = Random_RotationMultiplier * 90;
			//transform.Rotate(0, Random_Rotation, 0);
			this.transform.localEulerAngles = new Vector3(transform.rotation.x,Random_Rotation,transform.rotation.z);


		}
	}
}
