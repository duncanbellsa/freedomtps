﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_random_colour : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Renderer rend = GetComponent<Renderer>();
        //rend.material.shader = Shader.Find("Specular");
        rend.material.SetColor("_Color", new Color(Random.Range(0.00f,0.80f), Random.Range(0.00f, 0.80f), Random.Range(0.00f, 0.80f),1f));
        rend.material.SetFloat("_Metallic", (Random.Range(0.30f, 0.80f)));
        rend.material.SetFloat("_Glossiness", (Random.Range(0.30f, 0.80f)));

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
