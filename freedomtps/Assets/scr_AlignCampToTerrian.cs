﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_AlignCampToTerrian : MonoBehaviour {

    public bool HasPlayerVisted = false;
    public bool LoadedLevel = false;

    public bool MarketObject;

	public GameObject GameManager;


    // Use this for initialization
    void Start () {
		Ray ray = new Ray(this.transform.position,Vector3.down);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 1000))
		{
			Debug.DrawLine(ray.origin, hit.point);

		}

		//transform.position = hit.point;
		//transform.position = new Vector3(transform.position.x, transform.position.y+1.25f, transform.position.z);
		//transform.up = hit.normal;

		GameManager = GameObject.Find ("GameManager");

		//GameManager = GameObject.FindObjectOfType(typeof(scr_ResourceManagement)) as scr_ResourceManagement;


	}
	
	// Update is called once per frame
	void Update () {
       //if (HasPlayerVisted == true && LoadedLevel == false) {
       //     LoadNewLevel();

        //}
    }


    

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && HasPlayerVisted == false && MarketObject == false)
                {
			HasPlayerVisted = true;
            //LoadedLevel = true;

            LoadNewCamp();
        }

        if (other.gameObject.tag == "Player" && HasPlayerVisted == false && MarketObject == true)
        {
            HasPlayerVisted = true;
            //LoadedLevel = true;

            LoadNewMarket();
        }

    }


    
    void LoadNewCamp()
    {
		scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
		sn.ChangeToCamp();
		
		//GameManager.ChangeToCamp();

        Debug.Log("Player has entered" + this.name);


    }

    void LoadNewMarket()
    {
        scr_ResourceManagement sn = GameManager.GetComponent<scr_ResourceManagement>();
        sn.ChangeToMarket();

        //GameManager.ChangeToCamp();

        Debug.Log("Player has entered" + this.name);


    }
}
