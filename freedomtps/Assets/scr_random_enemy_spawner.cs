﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_random_enemy_spawner : MonoBehaviour {

    public GameObject EnemyGameObject;
    private int randomnum;

	// Use this for initialization
	void Start () {

        randomnum = Random.Range(1, 10);

        if (randomnum < 4)
        {
            Instantiate(EnemyGameObject, transform.position, Quaternion.identity);
        }


       
        Destroy(this);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
