﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_align_to_ground : MonoBehaviour {

    private Vector3 raycast_start_position;
	private bool FirstTime = true;

	// Use this for initialization
	void Start () {
		

        raycast_start_position = this.transform.position;
        raycast_start_position = new Vector3(transform.position.x, transform.position.y - 0.25f, transform.position.z);

        Ray ray = new Ray(raycast_start_position, Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000))
        {
            Debug.DrawLine(ray.origin, hit.point);

        }




		// Move and align object
        transform.position = hit.point;
        transform.position = new Vector3(transform.position.x, transform.position.y-0.25f, transform.position.z);

		transform.up = hit.normal;
		//transform.rotation = Quaternion.Euler(hit.normal.transform.rotation.x, hit.normal.transform.rotation.y, hit.normal.transform.rotation.z);

		FirstTime = false;
    }
	
	// Update is called once per frame
	void OnEnable () {
		if (FirstTime = false)
		{
			raycast_start_position = this.transform.position;
			raycast_start_position = new Vector3(transform.position.x, transform.position.y - 0.25f, transform.position.z);

			Ray ray = new Ray(raycast_start_position, Vector3.down);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1000))
			{
				Debug.DrawLine(ray.origin, hit.point);

			}




			// Move and align object
			transform.position = hit.point;
			transform.position = new Vector3(transform.position.x, transform.position.y-0.25f, transform.position.z);

			transform.up = hit.normal;
		
	}
}
}
