﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Boundary_Wall_Opacity : MonoBehaviour {

	//Opacity for wall
	public GameObject target;
	public float minDistance    = 1.0f;
	public float maxDistance    = 10.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float distance = Vector3.Distance(transform.position,target.transform.position);
		//float alpha = Mathf.InverseLerp(maxDistance, minDistance, Vector3.Distance(transform.position, target.transform.position));

		if (distance < maxDistance){
			this.GetComponent<MeshRenderer>().material.color = new Color(255f, 0f, 0f, 255f);
		}
		else this.GetComponent<MeshRenderer>().material.color = new Color(255f, 0f, 0f, 0f );

		//renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, alpha);
		Debug.Log(distance);
	}
}
