﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class scr_Generate_RandomTerrainTPS : MonoBehaviour
{

    //The higher the numbers, the more hills/mountains there are
    private float HM;

    //The lower the numbers in the number range, the higher the hills/mountains will be...
    private float divRange;

    //[MenuItem("Terrain/Generate Random Terrain")]
    //public static void CreateWizard(MenuCommand command)
    //{
    //ScriptableWizard.DisplayWizard("Generate Random Terrain", typeof(RandomTerrainGenerator));
    //}




    void Awake()
    {
            
            HM = Random.Range(1, 3);
            divRange = Random.Range(25,30);

            //Generate Terrain
            GameObject G = this.gameObject;
            if (G.GetComponent<Terrain>())
            {
                GenerateTerrain(G.GetComponent<Terrain>(), HM);
            }

        


    }

    public void Start()
    {
       
    }



    


  

   



    private float seed;
    //Our Generate Terrain function
    public void GenerateTerrain(Terrain t, float tileSize)
    {

        seed = Random.Range(0, 10000);
        //Mathf.PerlinNoise (seed, seed);

        //Heights For Our Hills/Mountains
        float[,] hts = new float[t.terrainData.heightmapWidth, t.terrainData.heightmapHeight];
        for (int i = 0; i < t.terrainData.heightmapWidth; i++)
        {
            for (int k = 0; k < t.terrainData.heightmapHeight; k++)
            {
                hts[i, k] = Mathf.PerlinNoise(seed + ((float)i / (float)t.terrainData.heightmapWidth) * tileSize, seed + ((float)k / (float)t.terrainData.heightmapHeight) * tileSize) / divRange;
            }
        }
        Debug.LogWarning("DivRange: " + divRange + " , " + "HTiling: " + HM);
        t.terrainData.SetHeights(0, 0, hts);


    }



}
