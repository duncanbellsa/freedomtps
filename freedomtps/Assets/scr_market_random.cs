﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_market_random : MonoBehaviour {

    private float MarketFuel;
	private float MarketAmmo;
    private float Multiplier;

    public Text MarketFuelText;
	public Text MarketAmmoText;

	public Button BuyFuel_BTN;
	public GameObject MarketObject; 
	 

	void OnEnable(){
        //randomize fuel
        //MarketFuel = 20;
        //Multiplier = Random.Range(1f, 5f);
        MarketFuel = 20 * Random.Range(1, 5);

        MarketAmmo = Random.Range(1.00f,100.00f);

	}

    // Use this for initialization
    void Start () {
        Screen.lockCursor = false;
        Cursor.visible = true;
        
    }
	
	// Update is called once per frame
	void Update () {
		
		MarketFuelText.text = (MarketFuel.ToString("F0"));
		MarketAmmoText.text = (MarketAmmo.ToString("F2"));
		
	}

	public void BuyFuel (){
		if (scr_ResourceManagement.ResourceMoney > 19 && MarketFuel > 19)
            {
            MarketFuel -= 20;
            scr_ResourceManagement.ResourceFuel += 20;
            scr_ResourceManagement.ResourceMoney -= 20;
        }

        
	}



	public void CloseMarket(){
		MarketObject.SetActive (false);
	}


}
